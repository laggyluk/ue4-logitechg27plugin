This plugin adds support for the Logitech G27 steering wheel controller to UE4.7. 

There are a few things to note:
1. Always make sure your target systems (ie the ones where you test the wheel) have the drivers installed. Forgetting that leads to unnecessary bug hunts. ;)

2. While the plugin *should* work in 4.8, i haven't had time to test this yet.