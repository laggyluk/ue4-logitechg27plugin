namespace LogitechG27Lib
{
	struct DeviceState 
	{
		bool ButtonStates[128];
		short ShifterPOV;

		//min value for Clutch, Brake and Accelerator is short.minValue, max is short.maxValue
		short ClutchValue; 
		short BrakeValue;
		short AcceleratorValue;

		//min value for steering is - as the pedals - between short.min and short.max
		short WheelPosition;

		//flag that tells the caller that we've not been initialized
		bool IsInitialized;
	};
#pragma once

	class G27Lib
	{
	private:
		static DeviceState GetDeviceState();

	public:
		G27Lib();

		/* Initialize the device */
		static void Initialize();
		
		/* set preferences */
		static void SetProperties(bool enableForce, int overallGain, int springGain, int damperGain, bool combinePedals, int wheelRange, bool enableDefaultSpring);

		/* Deactivate the initialized wheel */
		static void Deactivate();

		/* Update the wheel data and return the state, to be called whenever data from the device is required */
		static DeviceState Update();

		/* Check if the wheel is available */
		static int IsConnected();

		/* Set the wheel LEDs */
		static void PlayLEDS(int minRpm, int maxRpm, int currentRpm);

		// FORCE FEEDBACK

		// offset - where the wheel should be centered to. Range -1.0 - 1.0
		// saturation - intensity of the effect. Range 0.0 - 1.0
		// Percentage - slope of the effect, higher value means the full efect (saturation) is reached sooner. Range -1.0 - 1.0

		static void PlayBumpyRoadEffect(double magnitude);
		static void StopBumpyRoadEffect();
		static void PlayCarAirborne();
		static void StopCarAirborne();
		static void PlayConstantForce(int magnitudePercentage);
		static void StopConstantForce();
		static void PlayDamperForce(int coefficientPercentage);
		static void StopDamperForce();
		static void PlayDirtRoadEffect(int magnitudePercentage);
		static void StopDirtRoadEffect();
		static void PlayFrontalCollisionForce(int magnitudePercentage);
		static void PlaySideCollisionForce(int magnitudePercentage);
		static void PlaySlipperyRoadEffect(int magnitudePercentage);
		static void StopSlipperyRoadEffect();
		static void PlaySoftstopForce(int usableRangePercentage);
		static void StopSoftstopForce();
		static void PlaySpringForce(int offsetPercentage, int saturationPercentage, int coefficientPercentage);
		static void StopSpringForce();
		static void PlaySurfaceEffect(int periodicType, int magnitude, int frequency);
		static void StopSurfaceEffect();
	};
}