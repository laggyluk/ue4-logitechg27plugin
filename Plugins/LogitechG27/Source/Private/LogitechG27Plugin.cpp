// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "LogitechG27PluginPrivatePCH.h"
#include "LogitechG27Device.h"


class FLogitechG27Plugin : public ILogitechG27Plugin
{
private:
	FLogitechG27Device* _device;
public:
	/** Implements the rest of the IInputDeviceModule interface **/

	/** Creates a new instance of the IInputDevice associated with this IInputDeviceModule **/
	virtual TSharedPtr<class IInputDevice> CreateInputDevice(const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler);

	/** Called right after the module DLL has been loaded and the module object has been created **/
	virtual void StartupModule() override;

	/** Called before the module is unloaded, right before the module object is destroyed. **/
	virtual void ShutdownModule() override;

	virtual FLogitechG27Device GetDevice() override;
};

IMPLEMENT_MODULE(FLogitechG27Plugin, LogitechG27Plugin)

#define LOCTEXT_NAMESPACE "InputKeys"
const FKey ILogitechG27Plugin::LogitechG27_Wheel("Wheel");
const FKey ILogitechG27Plugin::LogitechG27_Accelerator("Accelerator");
const FKey ILogitechG27Plugin::LogitechG27_Brake("Brake");
const FKey ILogitechG27Plugin::LogitechG27_Clutch("Clutch");
const FKey ILogitechG27Plugin::LogitechG27_POV("POV");

const FKey ILogitechG27Plugin::LogitechG27_ShiftUpPaddle("ShiftUpPaddle");
const FKey ILogitechG27Plugin::LogitechG27_ShiftDownPaddle("ShiftDownPaddle");

const FKey ILogitechG27Plugin::LogitechG27_WheelButtonLeft1			("WheelButtonLeft1");
const FKey ILogitechG27Plugin::LogitechG27_WheelButtonLeft2			("WheelButtonLeft2");
const FKey ILogitechG27Plugin::LogitechG27_WheelButtonLeft3			("WheelButtonLeft3");
const FKey ILogitechG27Plugin::LogitechG27_WheelButtonRight1		("WheelButtonRight1");
const FKey ILogitechG27Plugin::LogitechG27_WheelButtonRight2		("WheelButtonRight2");
const FKey ILogitechG27Plugin::LogitechG27_WheelButtonRight3		("WheelButtonRight3");
const FKey ILogitechG27Plugin::LogitechG27_ShifterButtonBlackTop	("ShifterButtonBlackTop");
const FKey ILogitechG27Plugin::LogitechG27_ShifterButtonBlackLeft	("ShifterButtonBlackLeft");
const FKey ILogitechG27Plugin::LogitechG27_ShifterButtonBlackRight	("ShifterButtonBlackRight");
const FKey ILogitechG27Plugin::LogitechG27_ShifterButtonBlackBottom	("ShifterButtonBlackBottom");
const FKey ILogitechG27Plugin::LogitechG27_ShifterButtonRed1		("ShifterButtonRed1");
const FKey ILogitechG27Plugin::LogitechG27_ShifterButtonRed2		("ShifterButtonRed2");
const FKey ILogitechG27Plugin::LogitechG27_ShifterButtonRed3		("ShifterButtonRed3");
const FKey ILogitechG27Plugin::LogitechG27_ShifterButtonRed4		("ShifterButtonRed4");
const FKey ILogitechG27Plugin::LogitechG27_ShifterGear1				("ShifterGear1");
const FKey ILogitechG27Plugin::LogitechG27_ShifterGear2				("ShifterGear2");
const FKey ILogitechG27Plugin::LogitechG27_ShifterGear3				("ShifterGear3");
const FKey ILogitechG27Plugin::LogitechG27_ShifterGear4				("ShifterGear4");
const FKey ILogitechG27Plugin::LogitechG27_ShifterGear5				("ShifterGear5");
const FKey ILogitechG27Plugin::LogitechG27_ShifterGear6				("ShifterGear6");
const FKey ILogitechG27Plugin::LogitechG27_ShifterGearReverse		("ShifterGearReverse");


TSharedPtr<class IInputDevice> FLogitechG27Plugin::CreateInputDevice(const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler)
{
	UE_LOG(LogTemp, Warning, TEXT("Created new G27 input device!"));
	
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_Wheel, LOCTEXT("Wheel", "G27 Wheel"), FKeyDetails::GamepadKey | FKeyDetails::FloatAxis));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_Accelerator, LOCTEXT("Accelerator", "G27 Accelerator Pedal"), FKeyDetails::GamepadKey | FKeyDetails::FloatAxis));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_Brake, LOCTEXT("Brake", "G27 Brake Pedal"), FKeyDetails::GamepadKey | FKeyDetails::FloatAxis));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_Clutch, LOCTEXT("Clutch", "G27 Clutch Pedal"), FKeyDetails::GamepadKey | FKeyDetails::FloatAxis));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_POV, LOCTEXT("POV", "G27 POV Cross"), FKeyDetails::GamepadKey | FKeyDetails::FloatAxis));

	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShiftUpPaddle, LOCTEXT("ShiftUpPaddle", "G27 Shift Up Paddle"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShiftDownPaddle, LOCTEXT("ShiftDownPaddle", "G27 Shift Down Paddle"), FKeyDetails::GamepadKey));

	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_WheelButtonLeft1, LOCTEXT("WheelButtonLeft1", "G27 Wheel Button Left 1"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_WheelButtonLeft2, LOCTEXT("WheelButtonLeft2", "G27 Wheel Button Left 2"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_WheelButtonLeft3, LOCTEXT("WheelButtonLeft3", "G27 Wheel Button Left 3"), FKeyDetails::GamepadKey));

	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_WheelButtonRight1, LOCTEXT("WheelButtonRight1", "G27 Wheel Button Right 1"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_WheelButtonRight2, LOCTEXT("WheelButtonRight2", "G27 Wheel Button Right 2"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_WheelButtonRight3, LOCTEXT("WheelButtonRight3", "G27 Wheel Button Right 3"), FKeyDetails::GamepadKey));

	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterButtonBlackTop, LOCTEXT("ShifterButtonBlackTop,	", "G27 Shifter Black Button Top"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterButtonBlackLeft, LOCTEXT("ShifterButtonBlackLeft,", "G27 Shifter Black Button Left"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterButtonBlackRight, LOCTEXT("ShifterButtonBlackRight,", "G27 Shifter Black Button Right"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterButtonBlackBottom, LOCTEXT("ShifterButtonBlackBottom", "G27 Shifter Black Button Bottom"), FKeyDetails::GamepadKey));

	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterButtonRed1, LOCTEXT("ShifterButtonRed1", "G27 Shifter Red Button 1"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterButtonRed2, LOCTEXT("ShifterButtonRed2", "G27 Shifter Red Button 2"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterButtonRed3, LOCTEXT("ShifterButtonRed3", "G27 Shifter Red Button 3"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterButtonRed4, LOCTEXT("ShifterButtonRed4", "G27 Shifter Red Button 4"), FKeyDetails::GamepadKey));

	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterGear1,			LOCTEXT("ShifterGear1", "G27 Shifter 1"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterGear2,			LOCTEXT("ShifterGear2", "G27 Shifter 2"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterGear3,			LOCTEXT("ShifterGear3", "G27 Shifter 3"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterGear4,			LOCTEXT("ShifterGear4", "G27 Shifter 4"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterGear5,			LOCTEXT("ShifterGear5", "G27 Shifter 5"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterGear6,			LOCTEXT("ShifterGear6", "G27 Shifter 6"), FKeyDetails::GamepadKey));
	EKeys::AddKey(FKeyDetails(ILogitechG27Plugin::LogitechG27_ShifterGearReverse, LOCTEXT("ShifterGearReverse", "G27 Shifter R"), FKeyDetails::GamepadKey));

	//this code seems to add an actual input mapping to the project
	//const_cast<UInputSettings*>(GetDefault<UInputSettings>())->AddAxisMapping(FInputAxisKeyMapping("Wheel", ILogitechG27Plugin::LogitechG27_Wheel, 1.0f));

	// See GenericInputDevice.h for the definition of the IInputDevice we are returning here
	_device = new FLogitechG27Device(InMessageHandler);
	return MakeShareable(_device);
}

void FLogitechG27Plugin::StartupModule()
{
	// This code will execute after your module is loaded into memory (but after global variables are initialized, of course.)
	// Custom module-specific init can go here.

	UE_LOG(LogTemp, Warning, TEXT("LogitechG27Plugin initiated!"));

	// IMPORTANT: This line registers our input device module with the engine.
	//	      If we do not register the input device module with the engine,
	//	      the engine won't know about our existence. Which means 
	//	      CreateInputDevice never gets called, which means the engine
	//	      will never try to poll for events from our custom input device.
	IModularFeatures::Get().RegisterModularFeature(IInputDeviceModule::GetModularFeatureName(), this);
}


void FLogitechG27Plugin::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	UE_LOG(LogTemp, Warning, TEXT("LogitechG27Plugin shut down!"));	
	// Unregister our input device module
	IModularFeatures::Get().UnregisterModularFeature(IInputDeviceModule::GetModularFeatureName(), this);
}

FLogitechG27Device FLogitechG27Plugin::GetDevice()
{
	return *_device;
}