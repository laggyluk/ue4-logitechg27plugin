// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "LogitechG27PluginPrivatePCH.h"
#include "LogitechG27Device.h"
#include "IInputInterface.h"


FLogitechG27Device::FLogitechG27Device(const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler) : MessageHandler(InMessageHandler)
{
	// Initiate your device here	
	_g27 = G27Lib();
	_g27.Initialize();

	_buttonKeys[0]  = ILogitechG27Plugin::LogitechG27_ShifterButtonRed1;
	_buttonKeys[1]  = ILogitechG27Plugin::LogitechG27_ShifterButtonRed2;
	_buttonKeys[2]  = ILogitechG27Plugin::LogitechG27_ShifterButtonRed3;
	_buttonKeys[3]  = ILogitechG27Plugin::LogitechG27_ShifterButtonRed4;
	_buttonKeys[4]  = ILogitechG27Plugin::LogitechG27_ShiftUpPaddle;
	_buttonKeys[5]  = ILogitechG27Plugin::LogitechG27_ShiftDownPaddle;
	_buttonKeys[6]  = ILogitechG27Plugin::LogitechG27_WheelButtonRight1;
	_buttonKeys[7]  = ILogitechG27Plugin::LogitechG27_WheelButtonLeft1;
	_buttonKeys[8]  = ILogitechG27Plugin::LogitechG27_ShifterGear1;
	_buttonKeys[9]  = ILogitechG27Plugin::LogitechG27_ShifterGear2;
	_buttonKeys[10] = ILogitechG27Plugin::LogitechG27_ShifterGear3;
	_buttonKeys[11] = ILogitechG27Plugin::LogitechG27_ShifterGear4;
	_buttonKeys[12] = ILogitechG27Plugin::LogitechG27_ShifterGear5;
	_buttonKeys[13] = ILogitechG27Plugin::LogitechG27_ShifterGear6;
	_buttonKeys[14] = ILogitechG27Plugin::LogitechG27_ShifterGearReverse;
	_buttonKeys[15] = ILogitechG27Plugin::LogitechG27_ShifterButtonBlackTop;
	_buttonKeys[16] = ILogitechG27Plugin::LogitechG27_ShifterButtonBlackLeft;
	_buttonKeys[17] = ILogitechG27Plugin::LogitechG27_ShifterButtonBlackBottom;
	_buttonKeys[18] = ILogitechG27Plugin::LogitechG27_ShifterButtonBlackRight;
	_buttonKeys[19] = ILogitechG27Plugin::LogitechG27_WheelButtonRight2;
	_buttonKeys[20] = ILogitechG27Plugin::LogitechG27_WheelButtonLeft2;
	_buttonKeys[21] = ILogitechG27Plugin::LogitechG27_WheelButtonRight3;
	_buttonKeys[22] = ILogitechG27Plugin::LogitechG27_WheelButtonLeft3;
}

FLogitechG27Device::~FLogitechG27Device()
{
	// Close your device here
	//_g27.Deactivate();
}

void FLogitechG27Device::Tick(float DeltaTime)
{

}

bool FLogitechG27Device::IsDeviceAvailable()
{
	int isConnected = _g27.IsConnected();
	if (isConnected >= 0)
		return true;

	if (isConnected == -1)
	{
		UE_LOG(LogTemp, Warning, TEXT("SendControllerEvents: controller is not properly initialized."));
	}
	else if (isConnected == -2)
	{
		UE_LOG(LogTemp, Warning, TEXT("SendControllerEvents: controller is not accessible."));
	}
	return false;
}

void FLogitechG27Device::SendControllerEvents()
{
	// poll g27 lib and get status structure
	if (!IsDeviceAvailable())
		return;

	LogitechG27Lib::DeviceState currentState = _g27.Update();

	// check if status has changed compared to previous structure
	int stateChange = 0; //0 = no change, 1 = Button Pressed, 2 = Button Released;
	FKey currentButton;
	bool gearSelected = false;
	for (int i = 0; i < 23; i++)
	{
		if (_lastState.ButtonStates[i] && !currentState.ButtonStates[i])
			stateChange = 2;
		else if (!_lastState.ButtonStates[i] && currentState.ButtonStates[i])
			stateChange = 1;
		else //if previous == current
			stateChange = 0;

		currentButton = _buttonKeys[i];

		//send button event, if applicable
		switch (stateChange)
		{
		case 1:
			SendButtonDownEvent(currentButton);
			break;
		case 2:
			SendButtonUpEvent(currentButton);
			break;
		default:
			break;
		}
	}

	/* Send analog axis events here */	
	SendAxisEvent(ILogitechG27Plugin::LogitechG27_Wheel , currentState.WheelPosition / 32767.0f );
	SendAxisEvent(ILogitechG27Plugin::LogitechG27_Accelerator , currentState.AcceleratorValue / 32767.0f);
	SendAxisEvent(ILogitechG27Plugin::LogitechG27_Brake , currentState.BrakeValue / 32767.0f);
	SendAxisEvent(ILogitechG27Plugin::LogitechG27_Clutch , currentState.ClutchValue / 32767.0f);
	SendAxisEvent(ILogitechG27Plugin::LogitechG27_POV , currentState.ShifterPOV / 36000.0f);
	
	_lastState = currentState;
}

void FLogitechG27Device::SendButtonUpEvent(FKey button)
{
	FKeyEvent keyEvent(button, FSlateApplication::Get().GetModifierKeys(), 0, 0, 0, 0);
	FSlateApplication::Get().ProcessKeyUpEvent(keyEvent);
}

void FLogitechG27Device::SendButtonDownEvent(FKey button)
{
	FKeyEvent keyEvent(button, FSlateApplication::Get().GetModifierKeys(), 0, 0, 0, 0);
	FSlateApplication::Get().ProcessKeyDownEvent(keyEvent);
}

void FLogitechG27Device::SendAxisEvent(FKey axis, float value)
{
	FAnalogInputEvent analogEvent(axis, FSlateApplication::Get().GetModifierKeys(), 0, false, 0, 0, value);
	FSlateApplication::Get().ProcessAnalogInputEvent(analogEvent);
}

void FLogitechG27Device::SetMessageHandler(const TSharedRef< FGenericApplicationMessageHandler >& InMessageHandler)
{
	MessageHandler = InMessageHandler;
}

bool FLogitechG27Device::Exec(UWorld* InWorld, const TCHAR* Cmd, FOutputDevice& Ar)
{
	// Nothing necessary to do (boilerplate code to complete the interface)
	return false;
}

void FLogitechG27Device::SetChannelValue(int32 ControllerId, FForceFeedbackChannelType ChannelType, float Value)
{
	// Nothing necessary to do (boilerplate code to complete the interface)
}

void FLogitechG27Device::SetChannelValues(int32 ControllerId, const FForceFeedbackValues &values)
{
	// Nothing necessary to do (boilerplate code to complete the interface)
}