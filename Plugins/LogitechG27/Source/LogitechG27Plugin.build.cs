// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
using System.IO;

namespace UnrealBuildTool.Rules
{
    public class LogitechG27Plugin : ModuleRules
    {
        public LogitechG27Plugin(TargetInfo Target)
        {
            PublicIncludePaths.AddRange(
                new string[] {
					// ... add public include paths required here ...
					"LogitechG27/Public",
                    //@"H:\_GameDev_\TestForPlugin\Plugins\LogitechG27\ThirdParty\LogitechG27Lib\Includes",
				    }
                );

            PrivateIncludePaths.AddRange(
                new string[] {
					"LogitechG27/Private",
                    //"LogitechG27/ThirdParty/LogitechG27Lib/Includes"
					// ... add other private include paths required here ...
				}
                );

            PublicDependencyModuleNames.AddRange(
                new string[]
				{
					"Core",
                    "CoreUObject",
					"Engine",
					"Slate",
					"SlateCore",
					"InputCore",
					"InputDevice",
					// ... add other public dependencies that you statically link with here ...
				}
                );

            PrivateDependencyModuleNames.AddRange(
                new string[]
				{
					// ... add private dependencies that you statically link with here ...
				}
                );

            DynamicallyLoadedModuleNames.AddRange(
                new string[]
				{
					// ... add any modules that your module loads dynamically here ...
				}
                );

            AddThirdPartyPrivateStaticDependencies(Target,
                        new string[] 
                		{ 
                			// ... add any third party modules included with UE4 here ...
                		}
                        );
            LoadYourThirdPartyLibraries(Target);
        }

        public bool LoadYourThirdPartyLibraries(TargetInfo Target)
        {
            bool isLibrarySupported = false;

            // This will give oyu a reliative path to the module ../PsudoController/
            string ModulePath = Path.GetDirectoryName(RulesCompiler.GetModuleFilename(this.GetType().Name));
            // This will give you a relative path to ../PsudoController/ThirdParty/"LibraryDirName"/
            string MyLibraryPath = Path.Combine(ModulePath, @"..\ThirdParty", "LogitechG27Lib");

            // Use this to keep Win32/Win64/e.t.c. library files in seprate subdirectories
            string ArchitecturePath = "";

            // When you are building for Win64
            if (Target.Platform == UnrealTargetPlatform.Win64 && WindowsPlatform.Compiler == WindowsCompiler.VisualStudio2013)
            {
                // We will look for the library in ../PsudoController/ThirdParty/MyLibrary/Win64/VS20##/
                // ArchitecturePath = Path.Combine("Win64", "VS" + WindowsPlatform.GetVisualStudioCompilerVersionName());

                isLibrarySupported = true;
            }
            
            // When you are building for Win32
            //else if (Target.Platform == UnrealTargetPlatform.Win32 && WindowsPlatform.Compiler == WindowsCompiler.VisualStudio2013)
            //{
            //    // We will look for the library in ../PsudoController/ThirdParty/MyLibrary/Win32/VS20##/
            //    ArchitecturePath = Path.Combine("Win32", "VS" + WindowsPlatform.GetVisualStudioCompilerVersionName());

            //    isLibrarySupported = true;
            //}

            // Add mac/linux/mobile support in much the same way

            // If the current build architecture was supported by the above if statements
            if (isLibrarySupported)
            {
                // Add the architecture spacific path to the library files
                PublicAdditionalLibraries.Add(Path.Combine(MyLibraryPath, "Libraries", ArchitecturePath, "LogitechG27Lib.lib"));
                // Add a more generic path to the include header files
                PublicIncludePaths.Add(Path.Combine(MyLibraryPath, "Includes"));
            }

            // Defination lets us know whether we successfully found our library!
            Definitions.Add(string.Format("WITH_MY_LIBRARY_PATH_USE={0}", isLibrarySupported ? 1 : 0));

            return isLibrarySupported;
        }
    }
}


